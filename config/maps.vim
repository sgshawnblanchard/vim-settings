" Set map leader to space
let mapleader=' '

" Edit settings
nnoremap <leader>rc :e $SETTINGS_FILE<cr>

" Source Vim RC
nnoremap <leader>rC :so $MYVIMRC<cr>

" Visual mode replace without copy
vmap r "_dP

" Git status
nnoremap <leader>gs :Gstatus<cr>

" Git diff
nnoremap <leader>gd :Gdiff<cr>

" Flog
nnoremap <leader>gl :Flog<cr>

" Markdown shortcut for table format
nnoremap <space>tf :TableFormat<cr>

" Coc do hover
nnoremap <silent> K :call CocAction('doHover')<cr>

" Coc go to definition
nmap <silent> gd <plug>(coc-definition)

" Coc go to references
nmap <silent> gr <plug>(coc-references)

" Coc object rename
nmap <leader>rn <plug>(coc-rename)

" Coc object list
nnoremap <silent> <leader>ol :<c-u>CocList outline<cr>

" Coc format
vmap <leader>= <plug>(coc-format-selected)
nmap <leader>= <plug>(coc-format-selected)

" Coc fix autofix problem of current line
nmap <leader>qf <plug>(coc-fix-current)

" Coc tab completion
inoremap <silent><expr> <tab> pumvisible() ? '<c-n>' : CheckBackSpace() ? '<tab>' : coc#refresh()

" Coc shift-tab completion/delimitMate jump
imap <expr> <s-tab> pumvisible() ? '<c-p>' : "<plug>delimitMateS-Tab"

" Coc enter to complete
imap <expr> <cr> pumvisible() ? '<c-y>' : "<plug>delimitMateCR"

" Coc find file
nnoremap <leader>ff :CocList files<cr>

" Coc find recent file
nnoremap <leader>fh :CocList mru<cr>

" Coc find buffer
nnoremap <leader>fb :CocList buffers<cr>

" Coc find diagnostic
nnoremap <leader>fd :CocList diagnostics<cr>

" Coc grep
nnoremap <leader>fg :CocList grep<space>

" Coc grep for FIXME/TODO
nnoremap <leader>ft :CocList grep todo\|fixme<cr>

" Coc grep word
nnoremap <silent> <leader>fw  :exe 'CocList grep '.expand('<cword>')<cr>

" Shortcuts for CocNext and CocPrevious
nnoremap gn :CocNext<cr>
nnoremap gN :CocPrev<cr>

" NERDTree toggle
nnoremap <leader>n :NERDTreeToggle<cr>

" Vim over shortcut
nnoremap <leader>/ :OverCommandLine<cr>%s/

" New buffer in current area
nnoremap <leader>ee :enew<cr>

" New buffer in split
nnoremap <leader>es :new<cr>

" New buffer in vertical split
nnoremap <leader>ev :vnew<cr>

" Go to next buffer
nnoremap <tab> :bnext<cr>

" Go to previous buffer
nnoremap <s-tab> :bprevious<cr>

" Remove the current buffer and close current window
nnoremap <leader>bd :Sayonara<cr>

" Remove the current buffer and preserve current window
nnoremap <leader>bD :Sayonara!<cr>

" Remove all buffers except for the current one
nnoremap <leader>bo :BufOnly<cr>

" Split current file
nnoremap <leader>bs :split<cr>

" Vertical split current file
nnoremap <leader>bv :vsplit<cr>

" Create new tab
nnoremap <leader>tt :tabnew<cr>

" Go to next tab
nnoremap gt :tabnext<cr>

" Go to previous tab
nnoremap gT :tabprevious<cr>

" Update current buffer
nnoremap <leader>u :update<cr>

" Update all buffers
nnoremap <leader>U :wa<cr>

" Go to split below
nnoremap <c-j> <c-w>j

" Go to split above
nnoremap <c-k> <c-w>k

" Go to split left, add iTerm2 Keys `^h` to send escape sequence `[104;5u`
nnoremap <c-h> <c-w>h

" Go to split right
nnoremap <c-l> <c-w>l

" Make H and L go to beginning and end of line
nnoremap H ^
nnoremap L $
xnoremap H ^
xnoremap L $

" Keep search matches in the middle of the window
nnoremap n nzzzv
nnoremap N Nzzzv

" No arrow keys for you
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" Turn off search highlight
nnoremap ? :noh<cr>

" Toggle spell check
nnoremap <leader>sc :setlocal spell!<cr>

" Sort lines alphabetically
nnoremap <leader>sl :sort i<cr>
xnoremap <leader>sl :sort i<cr>

" Sort lines numerically
nnoremap <leader>sn :sort n<cr>
xnoremap <leader>sn :sort n<cr>

" Sort lines randomly
nnoremap <leader>sr :!sort -R<cr>
xnoremap <leader>sr :!sort -R<cr>

" Better indentation
nnoremap > >>
nnoremap < <<
xnoremap > >gv
xnoremap < <gv

" Toggle wrap
nnoremap <leader>wr :set invwrap<cr>:set wrap?<cr>

" Turn off ex mode
nnoremap Q <nop>

" Remove trailing whitespace
nnoremap <leader>xt :call StripTrailingWhitespace()<cr>

" Open with specified program
nnoremap <leader>ow :OpenWith<space>

" Open with default program
nnoremap <leader>oo :OpenWith Finder<cr>

" Open with Marked 2
nnoremap <leader>om :OpenWith Marked 2<cr>
