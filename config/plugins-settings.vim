" Python
let g:python_host_prog=$PYENV_ROOT . '/versions/neovim2/bin/python'
let g:python3_host_prog=$PYENV_ROOT . '/versions/neovim3/bin/python'

" JSX highlighting on Javascript files
let g:jsx_ext_required=0

" Javascript libraries
let g:used_javascript_libs='react'

" Markdown list without auto-indent
let g:vim_markdown_new_list_item_indent=0

" Remove indent guides from filetypes
let g:indent_guides_exclude_filetypes=['help', 'nerdtree']

" Git gutter signs
let g:gitgutter_sign_added='┃'
let g:gitgutter_sign_modified='┃'
let g:gitgutter_sign_removed='◢'
let g:gitgutter_sign_removed_first_line='◥'
let g:gitgutter_sign_modified_removed='◢'

" Git gutter no override column color
let g:gitgutter_override_sign_column_highlight=0

" DelimitMate auto-close, jump, space, character return and inside quotes
let g:delimitMate_autoclose=1
let g:delimitMate_jump_expansion=1
let g:delimitMate_expand_space=1
let g:delimitMate_expand_cr=2
let g:delimitMate_expand_inside_quotes=1
let g:delimitMate_balance_matchpairs=0

" Coc extensions
let g:coc_global_extensions=['coc-tsserver', 'coc-css', 'coc-json', 'coc-emoji', 'coc-lists']

" Coc airline integration
let g:airline_section_error='%{airline#util#wrap(airline#extensions#coc#get_error(),0)}'
let g:airline_section_warning='%{airline#util#wrap(airline#extensions#coc#get_warning(),0)}'

" Coc suggest settings
call coc#config('suggest', {
  \ 'disableKind': 1,
  \ 'disableMenu': 1,
  \ 'maxCompleteItemCount': 10
\ })

" Coc diagnostic settings
call coc#config('diagnostic', {
  \ 'enable': 0,
  \ 'errorSign': '•',
  \ 'hintSign': '•',
  \ 'infoSign': '•',
  \ 'warningSign': '•'
\ })

" Coc list source settings
let list_ignore='!**/{.git,package-lock.json,yarn.lock,Gemfile.lock}'
call coc#config('list.source', {
  \ 'files.args': ['--color', 'never', '--files', '--hidden', '--glob', list_ignore],
  \ 'grep.args': ['--smart-case', '--hidden', '--glob', list_ignore],
  \ 'grep.useLiteral': 0
\ })

" Coc buffer source settings
call coc#config('coc.source.buffer', {
  \ 'ignoreGitignore': 0
\ })

" Coc javascript settings
call coc#config('javascript', {
  \ 'format.insertSpaceBeforeFunctionParenthesis': 1,
  \ 'preferences.noSemicolons': 1,
  \ 'suggest.paths': 0,
  \ 'suggestionActions.enabled': 0
\ })

" Coc emoji source settings
call coc#config('coc.source.emoji', {
  \ 'filetypes': ['markdown', 'gitcommit']
\ })

" NERDTree open on GUI startup
let g:nerdtree_tabs_open_on_gui_startup=0

" NERDTree close on open file
let NERDTreeQuitOnOpen=1

" NERDTree arrow
let NERDTreeMinimalUI=1
let g:NERDTreeDirArrows=1
let g:NERDTreeCascadeSingleChildDir=0

" NERDTree ignore
let g:NERDTreeIgnore=['node_modules$[[dir]]', '\.DS_Store$', '.git$[[dir]]', '.next$[[dir]]']

" NERDTree split shortcuts
let NERDTreeMapOpenSplit='s'
let NERDTreeMapPreviewSplit='S'
let NERDTreeMapOpenVSplit='v'
let NERDTreeMapPreviewVSplit='V'

" NERDCommenter use space after delimiters
let g:NERDSpaceDelims=1

" NERDCommenter alternate Javascript comment for JSX
let g:NERDCustomDelimiters={
  \ 'javascript': { 'left': '//', 'right': '', 'leftAlt': '{/*', 'rightAlt': '*/}' },
\ }

" Airline auto-populate symbols
let g:airline_powerline_fonts=1

" Airline smarter tab line
let g:airline#extensions#tabline#enabled=1

" Airline only show filename in tab line
let g:airline#extensions#tabline#fnamemod=':t'

" Airline change section z
let g:airline_section_z='%4l/%L:%3v'
let g:airline#extensions#wordcount#enabled=0

" Indent guides enable
let g:indent_guides_enable_on_vim_startup=1

" Indent guides size
let g:indent_guides_guide_size=1

" Enable display colors for filetypes
let g:Hexokinase_ftAutoload=['css', 'scss']

" Sayonara behavior on certain file types
let g:sayonara_filetypes={ 'nerdtree': 'NERDTreeClose' }
